package mprojA_fgcLvTracket;

import java.util.ArrayList;

///----- Object that comprises each level of the rankTier Tree
public class rankTier {
	
	private	String		tierName	= "<TEMP>";	// Name of the Tier Object (for display & searching purposes)
	private	int			level		= MicroProjectA_FGCLevelTracker.LV_DEFAULT;
							// Calculated automatically (unless no children, in which case loaded from file)
							//===== TEMP VALUES ======
							// 0 | Default
							// 1 | Can do in Training Mode
							// 2 | Can Land In-Match
							// 3 | Reliably part of Toolkit
	
	private	ArrayList<rankTier> parentTiers;	// All the direct Parent objects of this Tier
	public	ArrayList<rankTier>	childrenTiers;	// All the direct Children of this Tier
	
	public	rankTier(String inName)	{
		setTierName(inName);
		
		setup();
	}
	
	//--- Initialization Methods
	public	void	setup()	{
		childrenTiers	= new ArrayList<rankTier>();
		parentTiers		= new ArrayList<rankTier>();
	}
	
	//--- Tier Name getter & setter
	public	String	getTierName()	{
		return tierName;
	}
	public	void	setTierName(String inName)	{
		tierName	= inName;
	}
	
	//--- Manual Level getter & setter
	//-- NOTE: generally calculateLevel() for returning, this only returns the manually-set value!
	public	int		getLevel()	{
		return	level;
	}
	public	void	setLevel(int inLevel)	{
		level	= inLevel;
	}

	//--- Child Objects: get from and add to ChildList
	//-- NOTE: Only add Parent objects, it'll handle the adding of Child objects automatically!
	public	rankTier	getChild(int index) {
		if(!childrenTiers.isEmpty())	{
			return	childrenTiers.get(index);
		}
		
		else	{
			if(MicroProjectA_FGCLevelTracker.WARN)	{	System.out.println("[W] rankTier.getChild() returned NULL");	}
			return null;
		}
	}
	public	void		addChild_AutoUseOnly(rankTier newChild)	{
		if(newChild != null)	{
			if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("[L] " + tierName + " P| " + newChild.tierName);	}
			childrenTiers.add(newChild);
			// Does NOT add Parent (prevents infinite loop) - MAKE SURE YOU ADD BY PARENT
		}
		else	{
			if(MicroProjectA_FGCLevelTracker.DEBUG)	{	System.out.println("[D] rankTier.adChild(); attempted to add NLL value");	}
		}
	}
	
	//--- Parent Objects: get from and add to ParentList
	public	rankTier	getParent(int index)	{
		if(!parentTiers.isEmpty()) {
			return	parentTiers.get(index);
		} else {
			if(MicroProjectA_FGCLevelTracker.WARN)	{	System.out.println("[W] rankTier -> getParent() returned NULL");	} 
			return	 null;
		}
	}
	public	void		addParent(rankTier inParent)	{
		if(inParent != null)	{
			if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("[L] " + tierName + " AddParent| " + inParent.tierName);	}
			parentTiers.add(inParent);
			inParent.addChild_AutoUseOnly(this);
		}
		else	{
			if(MicroProjectA_FGCLevelTracker.DEBUG)	{	System.out.println("[D] rankTier.addParent() attempted to add NULL value");	}
		}
	}

	//--- Recursively Calculate this rankTier's Level
	public	int	calculateLevel()	{
		//--	Recursively calculate the Level of this Tier from children objects, and pass the output up
		//--	If there is no child object (AKA you're at the endpoint), use the manually-set Level instead
			//- Might change from manually set level in the future to a checkbox system; TODO
		//--	Currently it just adds up the total Level (points) of the children, no algorithm to convert the "EXP" to actual "Levels"
		//-- TODO: Add an ACTUAL ALGORITHM to this
		
		if(childrenTiers.size() > 0)	{
			level	= MicroProjectA_FGCLevelTracker.LV_DEFAULT;
			for(int i = 0; i < childrenTiers.size(); i++)	{
				level	+=	(10 * childrenTiers.get(i).calculateLevel());
			}
		}
		
		if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("[L] " + tierName + " Level| " + level);	} 
		
		return	level;
	}
	
	//--- Recursively Output the rankTier tree to the Console
	//-- NOTE: This is a TEMPORARY feature, as there is no GUI implemented yet
	public	void	outputString(int tier)	{		
		if(!parentTiers.isEmpty()) {
			for(int i = 0; i < tier; i++)	{
				System.out.print("\t");
			}
		}
		
		System.out.println("[" + level + "]\t" + tierName);
		
		for(int i = 0; i < childrenTiers.size(); i++)	{
			childrenTiers.get(i).outputString(tier + 1);
		}
	}
	
	public	void	getBuildingRankTier(ArrayList<buildingRankTier> inList)	{
		for(int i = 0; i < childrenTiers.size(); i++)	{
			childrenTiers.get(i).getBuildingRankTier(inList);
		}
		
		buildingRankTier	tempBuild	= new buildingRankTier(tierName);
		
		if(level != MicroProjectA_FGCLevelTracker.LV_DEFAULT)	{
			tempBuild.setLevel(level);
		}
		
		if(parentTiers.size() != 0)	{
			for(int i = 0; i < parentTiers.size(); i++)	{
				tempBuild.addParent(parentTiers.get(i).getTierName());
			}
		}
		
		inList.add(tempBuild);
	}

}
