package mprojA_fgcLvTracket;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

// TODO: Manually Select Parents (Toggle ON/OFF, but require at least 1)
// TODO: A GUI, clearly
// TODO: Actual Level-calculating Algorithm
// TODO: Edit Levels & stuff of Leaf (endpoint) nodes

public class MicroProjectA_FGCLevelTracker {
	
	//--- Logging Variables (Toggle to determine what gets output to the console)
	public	static	final	boolean	ERROR	= true;		// Default: True
	public	static	final	boolean	WARN	= true;		// Default: True
	public	static	final	boolean	LOG		= false;	// Default: False
	public	static	final	boolean	DEBUG	= false;	// Default: False
	
	public	static	final	int	LV_DEFAULT	= 0;
	
	static	Scanner	input	= new Scanner(System.in);

	public static void main(String[] args) {
		menuMain();
		
		// TODO: Add GUI
	}
	
	public static void menuMain()	{
		int		selection	= 0;
		boolean	keepRunning	= true;
		
		while(keepRunning)	{
			System.out.println	(
				"\n\n\n"	+
				"||==========================================================="	+ "\n" +
				"||===============< FGC Level Tracker (ALPHA) >==============="	+ "\n" +
				"||==========================================================="	+ "\n" +
				"||"						+ "\n" +
				"||- [1] Open File"			+ "\n" +
				"||- [2] Create New File"	+ "\n" +
				"||"						+ "\n" +
				"||- <0> Exit Program"		+ "\n"
			);
			selection	= inputHandler_Int(0, 2);
			
			switch(selection)	{
				case 1:	listFiles();
					break;
				case 2: createFile();
					break;
				case 0: keepRunning	= false;
					break;
				default:	if(ERROR)	{	System.out.println("[ERROR] menuMain Defaulted on switch(selection)");	}
			}
		};	
	}
	
	public static void listFiles()	{
		System.out.println("========== Please Select the File to Open ==========");
		
		File	folder		= new File("InputFiles");
		
		//--- Create Folder if it doesn't exist
		if(!folder.exists())	{
			if(WARN)	{
				System.out.println(
						"[WARNING] Folder Not Found; New Folder Created."	+ "\n" +
						"--------- Folder \"InputFiles\" created in the same directory as this Program."	+ "\n\n"
				);
			}
			folder.mkdir();
		}
		
		File[]	listOfFiles	= folder.listFiles();	// Load list of Files in 'folder'
		int		selection	= 1;
		
		
		
		ArrayList<String>	fileSelectionList	= new ArrayList<String>();

		//--- Load Files into an ArrayList
	    for(int i = 0; i < listOfFiles.length; i++)	{
	      if((listOfFiles[i].isFile()) && (listOfFiles[i].getName().toLowerCase().endsWith(".txt")))	{	// Only load TXT files (for now?)
	        fileSelectionList.add(listOfFiles[i].getName());
	      }
	    }
	    
	    //--- List all the files with selection numerals next to them 
	    for(int i = 0; i < fileSelectionList.size(); i++)	{
	    	System.out.println("[" + (i+1) + "] " + fileSelectionList.get(i).substring(0, listOfFiles[i].getName().length() - 4));	// Hide the ".TXT" part of the filename
	    }
	    System.out.println("<0> Cancel and Return to Main Menu" + "\n");
	    selection	= inputHandler_Int(0, fileSelectionList.size());
	    
	    if(selection == 0)	{
	    	return;
	    } else	{
	    	logicHandler	mainProgram	= new logicHandler(fileSelectionList.get(selection - 1));	// Open Specific File (Currently just displays results immediately)
	    }
	}
	
	public static void createFile()	{
		final	String	newFileContent	=	// Saved "Base File Contents"
				"// Formatting Rules:\n" +
				"// [new rankTier's Name]\n" +
				"// (Saved Level) // must be '(-)' for it to be default!\n" +
				"// <Parent A of new rankTier>\n" +
				"// <Parent B of new rankTier>\n" +
				"// <Parent ...>\n\n";
		String	newFileName	= "";
		
		System.out.println("========== Creating New File ==========");
		System.out.print(">> Please Enter the Name of the File (no extension): ");
		newFileName	= inputHandler_String();
		
		try{
			File	newFile	= new File("InputFiles\\" + newFileName + ".txt");
			
			do{
				if(!newFile.exists())	{	// Create the File if it doesn't exist
					newFile.createNewFile();
					break;
				}
				else	{	// Throw an error and make them put in a different name if it does already exist
					System.out.print(
							"\n" + "That file already exists!" + "\n" +
							">> Please Enter the Name of the File (no extension): "
					);
					newFileName	= inputHandler_String();
					newFile	= new File("InputFiles\\" + newFileName + ".txt");
				}
			}while(true);
			
			//--- Write the actual file
			FileWriter		tempWriter			= new FileWriter(newFile.getAbsoluteFile());
			BufferedWriter	tempBufferedWriter	= new BufferedWriter(tempWriter);
			
			tempBufferedWriter.write(newFileContent);
			tempBufferedWriter.close();
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
		
		
	}

	//--- Ease-of-use method for gathering an Integer from the user
	public static int inputHandler_Int(int minNum, int maxNum)	{
		int 	temp	= 0;
		boolean	valid	= false;
		
		System.out.print(">> Please Enter a Number: ");
		
		while(!valid){
			try	{
				temp	= input.nextInt();
				valid	= true;
			} catch (Exception e)	{
				valid	= false;
			}
			
			if((temp < minNum) || (temp > maxNum) || (!valid)){
				System.out.print	(
					"\n" +
					">> Something was wrong with your input." +	"\n" +
					">> Please Enter a Number: "
				);
				
				valid	= false;
			}
		};
		
		System.out.println("\n\n");
		return temp;
	}

	//--- Ease-of-use method for gathering an Integer from the user
	public static String inputHandler_String()	{	// No Prompt
		String	temp	= "";
		boolean	valid	= false;
		
		while(!valid)	{
			try	{
				temp	= input.nextLine();
				valid	= true;
			}
			catch(Exception e)	{
				valid	= false;
			}
			
			if(temp.length() == 0)	{
				valid	= false;
			}
		}
		
		return	temp;
	}

}
