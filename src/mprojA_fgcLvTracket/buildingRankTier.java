package mprojA_fgcLvTracket;

import java.util.ArrayList;

///----- Temporary counterpart of the rankTier, used when loading from a file (load but don't link to non-existent objects, then use as linking instructions)
public class buildingRankTier {
	
	private	String	tierName	= "<TEMP>";		// Name of the Tier Object (for display)
	private	int		level		= 0;			// Calculated automatically (unless no children)
	private	ArrayList<String>	parentTiers;	// Parent object
	
	public	buildingRankTier(String inName)	{
		setTierName(inName);
		
		setup();
	}
	
	//--- Initialization Methods
	public	void	setup()	{
		parentTiers		= new ArrayList<String>();
	}
	
	//--- Tier Name getter & setter
	public	void	setTierName(String inName)	{
		tierName	= inName;
	}
	public	String	getTierName()	{
		return tierName;
	}
	
	//--- Parent List: Add or Access Parent objects 
	public	void	addParent(String inParent)	{
		parentTiers.add(inParent);
	}
	public	String	getParent(int index)	{
		if(!parentTiers.isEmpty()) {
			if(index < parentTiers.size())	{
				return	parentTiers.get(index);
			}
			else	{
				if(MicroProjectA_FGCLevelTracker.DEBUG)	{
					System.out.println("[D] buildingRankTier.getParent(): Attempt to access Invalid location");
				}
			}
		}
		else	{
			if(MicroProjectA_FGCLevelTracker.DEBUG)	{
				System.out.println("[D] buildingRankTier.getParent(): parentList is Empty!");
			}
		}
		
		return null;
	}
	public	int	getNumParents()	{
		return	parentTiers.size();
	}
	
	//--- Manually Set or Load this "rankTier"'s Level
	public	void	setLevel(int inLevel)	{
		level	= inLevel;
	}
	public	int	getLevel()	{
		return	level;
	}
	
	//~~~ DEBUG METHOD ~~~\\
	//--- Output this rankTier to Console (non-recursive, gotta loop it manually)
	public	void	outputString()	{		
		System.out.println(tierName + " | Lv: " + level);
		
		for(int i = 0; i < parentTiers.size(); i++)	{
			System.out.println("\tP | " + parentTiers.get(i));
		}
	}
}
