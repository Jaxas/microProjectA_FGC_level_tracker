package mprojA_fgcLvTracket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

///----- Handles the loading, calculating, and displaying of current rankTier Tree values
public class logicHandler {
	rankTier	topLevel;		// Very top node of the rankTier web/list/thing
	rankTier	currentLevel;
	String		fileTitle;

	///----- Life-cycle Methods
	public logicHandler(String fileToLoadWithExtension) {
			//--- Create the Top-Most level
		topLevel		= new rankTier("topLevel");
		
			//--- Load a file (Text, passed in from Main object
		loadFile(fileToLoadWithExtension);
		fileTitle	= fileToLoadWithExtension.substring(0, fileToLoadWithExtension.length() - 4);

			//--- Create the Current-Level Display
		currentLevel	= topLevel;
		
			//~~ [TEMP] Output the data to the console (no GUI yet)
		menuMain();
	}
	
	public	void	menuMain()	{
		int		selection	= 0;
		int		numChildren	= 0;
		boolean	keepRunning	= true;
		
		final	int	selOffset	= 5;
		
		while(keepRunning)	{
			System.out.println	(
				"\n\n\n"	+
				"|===============[ " + fileTitle + " ]==============="				+ "\n" +
				"|===============< Current: " + currentLevel.getTierName()  + " >"	+ "\n" +
				"|"	+ "\n" +
				"|- [1] Display Full Tree (from Top Node)"	+ "\n" +
				"|- [2] Display Tree from Current Level"	+ "\n" +
				"|- [3] Add New Sub-Tier"					+ "\n" +
				"|- [4] Write Output to File"				+ "\n" +
				"|"
			);
			
				//-- List Children Tiers
			for(numChildren = 0; numChildren < currentLevel.childrenTiers.size(); numChildren++)	{
				System.out.println("|- [" + (numChildren+selOffset) + "] GOTO: " + currentLevel.getChild(numChildren).getTierName());
			}
			if(numChildren == 0)	{
				System.out.println("|-- No Sub-Tiers Exist for this Tier; Leaf-Node Menu");
				System.out.println	("|- [5] Manually Set Level");
			}

				//-- Cancel and/or Return to Top
			if(currentLevel.equals(topLevel))	{
				System.out.println	(
					"|"								+ "\n" +
					"|- <0> Return to Main Menu"	+ "\n"
				);
			}
			else	{
				System.out.println	(
					"|"									+ "\n" +
					"|- <0> Return to the Top Level"	+ "\n"
				);
			}
			selection	= MicroProjectA_FGCLevelTracker.inputHandler_Int(0, numChildren + selOffset);
			
				//-- [1] Output Full Tree
			if(selection == 1)	{
				System.out.println("\n\n===============[ OUTPUT: " + fileTitle + " ]====================");
				topLevel.calculateLevel();
				topLevel.outputString(0);
			}
				//-- [2] Display Current Tree
			else if(selection == 2)	{
				System.out.println("\n\n==========[ OUTPUT: " + currentLevel.getTierName() + " ]");
				currentLevel.calculateLevel();
				currentLevel.outputString(0);
			}
				//-- [3] Create New Child Tier
			else if(selection == 3)	{
					createNewChildTier();
			}
				//-- [4] Save Output to File
			else if(selection == 4)	{
				writeFile(fileTitle + ".txt", false);	// TRUE = Overwrite, FALSE = Ask
			}
				//-- [5 (IF LEAF NODE)] Manually Edit LeafNode's Level
			else if((selection == 5) && (currentLevel.childrenTiers.size() == 0))	{
				System.out.print	(
					"|=====< Level Update: " + currentLevel.getTierName()	+ "\n" +
					"| 0 = Default Value"						+ "\n" +
					"| 1 = Can do Sometimes in Training Mode"	+ "\n" +
					"| 2 = Can do Reliably in Training Mode"	+ "\n" +
					"| 3 = Can land in Match"					+ "\n" +
					"| 4 = Reliable Part of Toolkit"			+ "\n" +
					"|"											+ "\n" +
					"|"
				);
				currentLevel.setLevel(MicroProjectA_FGCLevelTracker.inputHandler_Int(0, 4));
			}
				//-- <0> Return Up a Level
			else if(selection == 0)	{
				if(currentLevel.equals(topLevel))	{
					keepRunning	= false;	// Redundant?
					return;
				}
				else	{
					currentLevel	= topLevel;
				}
			}
				//-- [_] Select Child Tier
			else	{
				currentLevel	= currentLevel.getChild(selection - selOffset);
			}
		};
	}	
	
	public	rankTier	createNewChildTier()	{
		System.out.println	(
					"========== Create New Tier =========="	+ "\n" +
					">> Please Enter the New Tier's Name: "
				);
		
		rankTier	newTier = new rankTier(MicroProjectA_FGCLevelTracker.inputHandler_String());
		
		newTier.addParent(currentLevel);
		
		return	newTier;
	}
	
	//--- Load data from a Text File
	/* Formatting Rules:
	 * [new rankTier's Name]
	 * (Saved Level) // must be "(-)" for it to be default!
	 * <Parent A of new rankTier>
	 * <Parent B of new rankTier> // Can have however many 
	 */
	public	void	loadFile(String fileNameWithExtension)	{
		try	(
			BufferedReader reader = new BufferedReader(new FileReader("InputFiles\\" + fileNameWithExtension)))	{
			
			ArrayList<buildingRankTier>	tempList	= new ArrayList();	// Temporary list of string-only versions of rankTiers (load without linking)
			String inLine = reader.readLine();
			
			if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("[LOG] ===== File Input Confirmation =====");	}
			
			//-- Iterate through the file one line at a time
			//-- Any new [tierName] passed in ends the last object
			//-- TODO: Automate input handling, so users can't screw stuff up
			while(inLine != null)	{
				if(inLine.isEmpty() || inLine.startsWith("//"))	{	// Blank Line or Comment = Skip
					if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.print("---");	}
				}
				else if(inLine.startsWith("["))	{	// [___] = New Tier w/ Tier's Name
					if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.print("- [tierName] ");	}
					tempList.add(new buildingRankTier(inLine.substring(1, inLine.length() - 1)));	// Create the new RankTier to be initialized, and pass in its Name					
				}
				else if(inLine.startsWith("("))	{	// (-) = Uninitialized Level, (##) = Manual Saved Level
					if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.print("- (Saved Level) ");	}
					if(!inLine.contains("-"))	{
						tempList.get(tempList.size() - 1).setLevel(Integer.parseInt(inLine.substring(1, inLine.length() - 1)));
					} else	{
						tempList.get(tempList.size() - 1).setLevel(0);
					}
				}
				else if(inLine.startsWith("<"))	{	// <___> = Parent Object
					if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.print("- <Parent Tier> ");	}
					tempList.get(tempList.size() - 1).addParent(inLine.substring(1, inLine.length() - 1));
				}
				
				if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("| " + inLine);	}
				inLine	= reader.readLine();
			}
			
		if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("\n[L] Finished Reading from File\n");	}
		
		buildFinalRankTierList(tempList);
			
		} catch (Exception e)	{
			e.printStackTrace();
		}
	}

	public	void	writeFile(String fileNameWithExtension, boolean overwrite)	{
		ArrayList<buildingRankTier>	stringList	= new ArrayList<buildingRankTier>();
		
		//-- Build StringList (Recursive Method)
		topLevel.getBuildingRankTier(stringList);
		
		//-- Remove Duplicates Created by going through the multi-directional linked list
		for(int i = 0; i < stringList.size(); i++)	{
			for(int a = (i + 1); a < stringList.size(); a++)	{
				if(stringList.get(i).getTierName().equals(stringList.get(a).getTierName()))	{
					stringList.remove(a);
					a--;
				}
			}
			if(stringList.get(i).getTierName().equals("Top"))	{
				stringList.remove(i);
			}
		}
		
		//-- Confirm Overwrite
		File	newFile	= new File("InputFiles\\" + fileNameWithExtension);	// Used for checking if the file already exists		
		if(!overwrite && newFile.exists())	{	// If Overwrite isn't set and the file to overwrite actaully exists
			System.out.println("== The File " + fileNameWithExtension + " already exists, would you like to overwrite it? [1 = YES | 0 = NO]");
			int	selection	= MicroProjectA_FGCLevelTracker.inputHandler_Int(0, 1);
			
			if(selection == 0)	{	// Return to Main Menu if Not Overwriting
				System.out.println("\n\n[NOTE] File Overwrite Cancelled, Returning to Main Menu\n\n");
			}
			
			// Just continue to saving otherwise
		}
		
		//--- Actually Write to the File
		try	(
			BufferedWriter writer = new BufferedWriter(new FileWriter("InputFiles\\" + fileNameWithExtension)))	{
				
			if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("====================");	}
			for(int i = 0; i < stringList.size(); i++)	{
				if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println(	"\n" + "[" + stringList.get(i).getTierName() + "]"	);	}
				writer.write("\n" + "[" + stringList.get(i).getTierName() + "]" + "\n");
				
				if(	(stringList.get(i).getLevel() != MicroProjectA_FGCLevelTracker.LV_DEFAULT)	// Only write if it's a leaf-node value, which is currently 0-4
						&& (stringList.get(i).getLevel() < 10))	{
					if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println(	"(" + stringList.get(i).getLevel() + ")"	);	}
					writer.write("(" + stringList.get(i).getLevel() + ")" + "\n");
				}
				else	{
					if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println(	"(-)"	);	}
					writer.write("(-)" + "\n");
				}
				
				for(int a = 0; a < stringList.get(i).getNumParents(); a++) {
					if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println(	"<" + stringList.get(i).getParent(a) + ">"	);	}
					writer.write("<" + stringList.get(i).getParent(a) + ">" + "\n");
				}
				
			}
		}
		catch(Exception e)	{
			e.printStackTrace();
		}
	}
	
	//--- Build the real rankTier list from the string-only, unlinked version
	public	void	buildFinalRankTierList(ArrayList<buildingRankTier> buildingList)	{
		//--- Initialize Top Node of rankTier List
		topLevel	= new rankTier("Top");
		
		//--- Create a temporary list of all Built rankTier objects
		ArrayList<rankTier>	tempBuiltList	= new ArrayList();
		if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("[L] TopLevel Object & tempBuiltList Created");	}
		
		//--- Create All Tiers in the first place, and add Levels if needed
		//-- NOTE: tempBuiltList intentionally mirrors the order of the buildingRankTier list!
		for(int i = 0; i < buildingList.size(); i++)	{
			tempBuiltList.add(new rankTier(buildingList.get(i).getTierName()));
			if(buildingList.get(i).getLevel() != 0)	{
				tempBuiltList.get(tempBuiltList.size() - 1).setLevel(buildingList.get(i).getLevel());
			}
		}
		if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("[L] Tier Objects Created");	}
		
		//--- Once they're all created, pair their Parent objects
		//-- Ignore children so there are no duplicates - if there are mismatches, that's its own issue!
		//-- Again, note that both the buildingList and the tempBuiltList are in the SAME order, so this works!
		//-- TODO: Implement different error-handling methods
		for(int i = 0; i < buildingList.size(); i++) {
			int a = 0;
			while(buildingList.get(i).getParent(a) != null)	{
				//-- Catch if the Parent is listed as TOP (which isn't in either array!) 
				if(buildingList.get(i).getParent(a).equals("Top"))	{
					tempBuiltList.get(i).addParent(topLevel);
				}
				//-- Search list of all rankTiers to pair
				// *Cries in efficiency*
				else	{
					tempBuiltList.get(i).addParent(findByName(tempBuiltList, buildingList.get(i).getParent(a)));
				}
				
				a++;
			}
		}
		
		if(MicroProjectA_FGCLevelTracker.LOG)	{	System.out.println("[L] Tier Objects Linked");	}
	}
	
	//--- Find and return a rankTier by name (searchString) in the list (searchList) 
	public	rankTier	findByName(ArrayList<rankTier> searchList, String searchString)	{
		for(int i = 0; i < searchList.size(); i++)	{
			if(MicroProjectA_FGCLevelTracker.DEBUG)	{	System.out.println("[D] " + searchList.get(i).getTierName() + " =?= " + searchString);	}
			if(searchList.get(i).getTierName().equals(searchString))	{
				return	searchList.get(i);
			}
		}
		
		return null;	// rankTier with name <searchString> not found
	}
}